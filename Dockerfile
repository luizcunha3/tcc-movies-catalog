FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

ENV MONGODB_URI 'mongodb://mongodb/tcc'

CMD ["npm", "start"]