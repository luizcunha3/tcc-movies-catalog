const mongoose = require('mongoose');
const config = require('../config');
const NinetiesMovies = require('../models/ninetiesMovies');
const RentalMovies = require('../models/rentalMovies');

const handleConnection = _ => {
    return new Promise(((resolve, reject) => {
        mongoose.Promise = global.Promise;
        mongoose.connect(config.db.uri, {useNewUrlParser: true});
        const db = mongoose.connection;

        db.on('error', (err) => {
            reject('Could not connect to mongo')
        });

        db.once('open', () => {
            resolve(true)
        });
    }))

};

const getMovies = (collection, params) => {
    if(params.id){
        return collection.findById(params.id);
    } else {
        let query  = {};
        if(params.name){
            query.name = { $regex: '.*' + params.name + '.*', $options: 'i'}
        }
        if(params.year){
            query.year = { $regex: '.*' + params.year + '.*', $options: 'i'}
        }
        if(params.id){
            query._id = `ObjectId(${params.id})`
        }

        return collection.find(query)
    }

};

module.exports = function(server) {
    server.get('/', (req, res, next) => {
        res.send(200, "Catalog is running")
    })
    server.get('/movies', (req, res) => {
        handleConnection()
            .then(status => {
                return getMovies(NinetiesMovies, req.params)
            })
            .then(movies => {
                res.send(200, movies);
                mongoose.disconnect()
            })
            .catch(err => {
                console.log(err);
                mongoose.disconnect()
            });
    });
    server.get('/rentals', (req, res) => {
        handleConnection()
            .then(status => {
                return getMovies(RentalMovies, req.params)
            })
            .then(movies => {
                res.send(200, movies);
                mongoose.disconnect()
            })
            .catch(err => {
                console.log(err);
                mongoose.disconnect()
            });
    })
};