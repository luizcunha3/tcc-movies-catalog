const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');


const RentalMoviesSchema = new mongoose.Schema(
    {
        "name": {
            "type": "String"
        },
        "year": {
            "type": "String"
        },
        "casting": {
            "director": {
                "type": "String"
            },
            "actors": {
                "type": [
                    "String"
                ]
            }
        },
        "synopsis": {
            "type": "String"
        }
    }

);

RentalMoviesSchema.plugin(timestamps);

const RentalMoviesModel= mongoose.model('movies-rent', RentalMoviesSchema, 'movies-rent');
module.exports = RentalMoviesModel;