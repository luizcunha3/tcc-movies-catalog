const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');


const NinetiesMoviesSchema = new mongoose.Schema(
    {
        "name": {
            "type": "String"
        },
        "year": {
            "type": "String"
        },
        "casting": {
            "director": {
                "type": "String"
            },
            "actors": {
                "type": [
                    "String"
                ]
            }
        },
        "synopsis": {
            "type": "String"
        }
    }

);

NinetiesMoviesSchema.plugin(timestamps);

const NinetiesMovieModel = mongoose.model('movies-theater', NinetiesMoviesSchema, 'movies-theater');
module.exports = NinetiesMovieModel;